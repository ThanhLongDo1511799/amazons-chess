# Import pygame and libraries
from pygame.locals import *
import pygame
from random import randrange
import os
import time

# Import pygameMenu
import pygameMenu
from pygameMenu.locals import *

# import global const
from const import *

# import class which using in game
from square import *
from point import *
from board import *

#import AI
from hardAI import *
from easyAI import *

#import network class
from network import *

ABOUT = ['The game of Amazons',
         'Author: Team 2, game programming course',
         'University: Bach Khoa university',
         PYGAMEMENU_TEXT_NEWLINE,
         'Email: long.dodg@gmail.com']

# -----------------------------------------------------------------------------
# Init pygame
pygame.init()
os.environ['SDL_VIDEO_CENTERED'] = '1'

# Create pygame screen and objects
surface = pygame.display.set_mode(WINDOW_SIZE)
pygame.display.set_caption('The game of amazons')
clock = pygame.time.Clock()
dt = 1 / FPS

# Global variables
DIFFICULTY = ['EASY']
MOVE_FIRST = ['YES']

# -----------------------------------------------------------------------------

def change_difficulty(d):
    """
    Change difficulty of the game.

    :return: 
    """
    # print('Selected difficulty: {0}'.format(d))
    DIFFICULTY[0] = d

def change_moveFirst(d):
    """
    choose your turn is first or second

    :return: 
    """
    # print('Selected difficulty: {0}'.format(d))
    MOVE_FIRST[0] = d


def random_color():
    """
    Return random color.

    :return: Color tuple
    """
    return randrange(0, 255), randrange(0, 255), randrange(0, 255)


def personModePlay(difficulty, font):
    """
    person mode play function

    :param difficulty: Difficulty of the game
    :param font: Pygame font
    :return: None
    """
    difficulty = difficulty[0]
    assert isinstance(difficulty, str)

    turnPlayer1 = True
    nboard = board()
    
    #disable menu
    main_menu.disable()
    main_menu.reset(1)

    while True:

        # Clock tick
        clock.tick(60)

        # Application events
        playevents = pygame.event.get()
        for e in playevents:
            if e.type == QUIT:
                exit()
            elif e.type == KEYDOWN:
                if e.key == K_ESCAPE and main_menu.is_disabled():
                    main_menu.enable()

                    # Quit this function, then skip to loop of main-menu on line 217
                    return
            elif e.type == pygame.MOUSEBUTTONDOWN:
                # print(turnPlayer1)
                if turnPlayer1:
                    turnPlayer1 = nboard.select(
                        pygame.mouse.get_pos(), 'w', turnPlayer1)
                else:
                    turnPlayer1 = nboard.select(
                        pygame.mouse.get_pos(), 'b', turnPlayer1)

        # Pass events to main_menu
        main_menu.mainloop(playevents)

        # Continue playing
        nboard.update(surface)

        # surface.fill(bg_color)
        # surface.blit(f, ((WINDOW_SIZE[0] - f_width) / 2, WINDOW_SIZE[1] / 2))
        pygame.display.flip()


def AImodePlay(difficulty, font):
    """
    AI mode play function

    :param difficulty: Difficulty of the game
    :param font: Pygame font
    :return: None
    """
    difficulty = difficulty[0]
    assert isinstance(difficulty, str)

    # init player, AI, board
    bot = None
    personTurn = True
    nboard = board()

    #choose difficulty
    if difficulty == 'EASY':
        f = font.render('easy AI!!!', 1, COLOR_WHITE)
        bot = easyAI('b')
    elif difficulty == 'HARD':
        f = font.render('hard AI!!!', 1, COLOR_WHITE)
        bot = hardAI('b')
    else:
        raise Exception('Unknown difficulty {0}'.format(difficulty))

    # Draw random color and text
    f_width = f.get_size()[0]

    #disable menu
    main_menu.disable()
    main_menu.reset(1)
    
    while True:

        # Clock tick
        clock.tick(60)

        playevents = pygame.event.get()
        for e in playevents:
            if e.type == QUIT:
                exit()
            elif e.type == KEYDOWN:
                if e.key == K_ESCAPE and main_menu.is_disabled():
                    main_menu.enable()

                    # Quit this function, then skip to loop of main-menu on line 217
                    return

        if personTurn:
            # Application events
            for e in playevents:
                if e.type == pygame.MOUSEBUTTONDOWN:                  
                    personTurn = nboard.select(
                        pygame.mouse.get_pos(), 'w', personTurn)
        else:
            tempState = nboard.convert2matrix()
            move = bot.nextMove(tempState)
            # print(move)
            if not move:
                f = font.render('you win!!!', 1, COLOR_WHITE)
            else:
                for i in range(3):
                    pos = (move[i][1] * (SQUARE_SIZE + 5), move[i][0] * (SQUARE_SIZE + 5))
                    personTurn = nboard.select(pos,'b', personTurn)
        
                    # Continue playing
                    nboard.update(surface)

                    # surface.fill(bg_color)
                    surface.blit(f, ((WINDOW_SIZE[0] - f_width) / 2, WINDOW_SIZE[1] / 2))
                    pygame.display.flip()
                    time.sleep(1)
        
        # Pass events to main_menu
        main_menu.mainloop(playevents)

        # Continue playing
        nboard.update(surface)

        # surface.fill(bg_color)
        surface.blit(f, ((WINDOW_SIZE[0] - f_width) / 2, WINDOW_SIZE[1] / 2))
        pygame.display.flip()


def networkModePlay(move_First, font):
    """
    person mode play function

    :param move_first: your turn is first if move_first==true
    :param font: Pygame font
    :return: None
    """
    move_First = move_First[0]
    # print(move_First)

    assert isinstance(move_First, str)

    # Draw random color and text
    # bg_color = random_color()
    # f_width = f.get_size()[0]

    nboard = board()
    port = 12345
    turnPlayer = True
    player = None 

    if move_First == 'YES':
        player = Server(port)
        player.accept()
    else:
        player = Client(port)

    # main_menu as the menu that will check all your input.
    main_menu.disable()
    main_menu.reset(1)
    while True:
        
        # Clock tick
        clock.tick(60)

        #receive data
        #white wait black play
        if move_First == 'YES' and (not turnPlayer):
            # print("data receive:", player.receive())
            print(nboard.move)
            nboard.changeBoard(player.receive())
            turnPlayer = True
        #black wait white play
        elif move_First == 'NO' and turnPlayer:
            # print("data receive:", player.receive())
            print(nboard.move)
            nboard.changeBoard(player.receive())            
            turnPlayer = False

        # Application events
        playevents = pygame.event.get()
        for e in playevents:
            if e.type == QUIT:
                exit()
            elif e.type == KEYDOWN:
                if e.key == K_ESCAPE and main_menu.is_disabled():
                    main_menu.enable()

                    # Quit this function, then skip to loop of main-menu on line 217
                    return
            elif e.type == pygame.MOUSEBUTTONDOWN:
                if move_First == 'YES' and turnPlayer:
                    turnPlayer = nboard.select(pygame.mouse.get_pos(), 'w', turnPlayer)
                    if not turnPlayer:
                        player.send(nboard.move)
                elif move_First == 'NO' and (not turnPlayer):
                    turnPlayer = nboard.select(pygame.mouse.get_pos(), 'b', turnPlayer)
                    if turnPlayer:
                        player.send(nboard.move)
                        
        # Pass events to main_menu
        main_menu.mainloop(playevents)

        # Continue playing
        nboard.update(surface)

        # surface.fill(bg_color)
        # surface.blit(f, ((WINDOW_SIZE[0] - f_width) / 2, WINDOW_SIZE[1] / 2))
        pygame.display.flip()


def main_background():
    """
    Function used by menus, draw on background while menu is active.

    :return: None
    """
    surface.fill(COLOR_BACKGROUND)


# -----------------------------------------------------------------------------
# PLAY MENU
play_menu = pygameMenu.Menu(surface,
                            bgfun=main_background,
                            color_selected=COLOR_WHITE,
                            font=pygameMenu.fonts.FONT_BEBAS,
                            font_color=COLOR_BLACK,
                            font_size=30,
                            menu_alpha=100,
                            menu_color=MENU_BACKGROUND_COLOR,
                            menu_height=int(WINDOW_SIZE[1] * 0.6),
                            menu_width=int(WINDOW_SIZE[0] * 0.6),
                            onclose=PYGAME_MENU_DISABLE_CLOSE,
                            option_shadow=False,
                            title='Play menu',
                            window_height=WINDOW_SIZE[1],
                            window_width=WINDOW_SIZE[0]
                            )
# When pressing return -> play(DIFFICULTY[0], font)
play_menu.add_option('Start', AImodePlay, DIFFICULTY,
                     pygame.font.Font(pygameMenu.fonts.FONT_FRANCHISE, 30))
play_menu.add_selector('Select difficulty', [('Easy', 'EASY'),
                                             ('Hard', 'HARD')],
                       onreturn=None,
                       onchange=change_difficulty)
play_menu.add_option('Return to main menu', PYGAME_MENU_BACK)

# ABOUT MENU
about_menu = pygameMenu.TextMenu(surface,
                                 bgfun=main_background,
                                 color_selected=COLOR_WHITE,
                                 font=pygameMenu.fonts.FONT_BEBAS,
                                 font_color=COLOR_BLACK,
                                 font_size_title=30,
                                 font_title=pygameMenu.fonts.FONT_8BIT,
                                 menu_color=MENU_BACKGROUND_COLOR,
                                 menu_color_title=COLOR_WHITE,
                                 menu_height=int(WINDOW_SIZE[1] * 0.6),
                                 menu_width=int(WINDOW_SIZE[0] * 0.6),
                                 onclose=PYGAME_MENU_DISABLE_CLOSE,
                                 option_shadow=False,
                                 text_color=COLOR_BLACK,
                                 text_fontsize=20,
                                 title='About',
                                 window_height=WINDOW_SIZE[1],
                                 window_width=WINDOW_SIZE[0]
                                 )
for m in ABOUT:
    about_menu.add_line(m)
about_menu.add_line(PYGAMEMENU_TEXT_NEWLINE)
about_menu.add_option('Return to menu', PYGAME_MENU_BACK)

#NETWORK MENU
network_menu = pygameMenu.Menu(surface,
                            bgfun=main_background,
                            color_selected=COLOR_WHITE,
                            font=pygameMenu.fonts.FONT_BEBAS,
                            font_color=COLOR_BLACK,
                            font_size=30,
                            menu_alpha=100,
                            menu_color=MENU_BACKGROUND_COLOR,
                            menu_height=int(WINDOW_SIZE[1] * 0.6),
                            menu_width=int(WINDOW_SIZE[0] * 0.6),
                            onclose=PYGAME_MENU_DISABLE_CLOSE,
                            option_shadow=False,
                            title='Play menu',
                            window_height=WINDOW_SIZE[1],
                            window_width=WINDOW_SIZE[0]
                            )
# When pressing return -> play(DIFFICULTY[0], font)
network_menu.add_option('Start', networkModePlay, MOVE_FIRST,
                     pygame.font.Font(pygameMenu.fonts.FONT_FRANCHISE, 30))
# network_menu.add_selector('Select difficulty', [('Easy', 'EASY'),
#                                              ('Hard', 'HARD')],
#                        onreturn=None,
#                        onchange=change_difficulty)
network_menu.add_selector('do you want move first?',[('yes', 'YES'),('no', 'NO')],
                        onreturn=None,
                        onchange=change_moveFirst)
network_menu.add_option('Return to main menu', PYGAME_MENU_BACK)

# MAIN MENU
main_menu = pygameMenu.Menu(surface,
                            bgfun=main_background,
                            color_selected=COLOR_WHITE,
                            font=pygameMenu.fonts.FONT_BEBAS,
                            font_color=COLOR_BLACK,
                            font_size=30,
                            menu_alpha=100,
                            menu_color=MENU_BACKGROUND_COLOR,
                            menu_height=int(WINDOW_SIZE[1] * 0.6),
                            menu_width=int(WINDOW_SIZE[0] * 0.6),
                            onclose=PYGAME_MENU_DISABLE_CLOSE,
                            option_shadow=False,
                            title='Main menu',
                            window_height=WINDOW_SIZE[1],
                            window_width=WINDOW_SIZE[0]
                            )
main_menu.add_option('P - AI', play_menu)
main_menu.add_option('p - p', personModePlay, DIFFICULTY,
                     pygame.font.Font(pygameMenu.fonts.FONT_FRANCHISE, 30))
main_menu.add_option('P - P(network)', network_menu)
main_menu.add_option('About', about_menu)
main_menu.add_option('Quit', PYGAME_MENU_EXIT)

# -----------------------------------------------------------------------------
# Main loop
while True:

    # Tick
    clock.tick(60)

    # Application events
    events = pygame.event.get()
    for event in events:
        if event.type == QUIT:
            exit()

    # Main menu
    main_menu.mainloop(events)

    # Flip surface
    pygame.display.flip()
