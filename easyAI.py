
import threading
import time
from collections import deque
import sys
import random

# ======================== AI class =======================================
# state matrix and coordinate space
# ...w..w...
# ..........
# ..........
# w........w
# ..........
# ..........
# b........b
# ..........
# ..........
# ...b..b...
# 0------>y
# |
# |
# |
# x


class easyAI:
    def __init__(self, str_name):
        self.str = str_name

    def __str__(self):
        return self.str

    def isLose(self, state, str_name):
        tempStr = self.str
        self.str = str_name

        move_list = self.generate_move(state)
        self.str = tempStr

        if not move_list:
            return True
        return False

    def nextMove(self, state):
            # self.board_print(state)
            # print(state[6][0])
            # print("")
            # sys.exit()
        start = time.time()
        move_list = self.generate_move(state)
        # file=open("test.txt","w")
        # file.write(str(move_list))
        # file.close()
        # sys.exit()

        if not move_list:  # lose
            return []

        resultMove = move_list[random.randint(0, len(move_list) - 1)]
        return resultMove

    # generate all possible movement
    def generate_move(self, state):

        queen_list = deque()  # list for saving queen position
        move_list = deque()  # movement of queen
        resultList = deque()  # all posible movement

        for x in range(10):
            for y in range(10):
                if state[x][y] == self.str:
                    queen_list.append((x, y))

        # self.board_print(state)
        # print(queen_list)
        # for (x,y) in queen_list:
        #     print(x,y)
        #     print(state[x][y])
        # print(state[9][6])
        # sys.exit()
        while queen_list:
            (x, y) = queen_list.popleft()
            move = [[x, y]]

            # print(state[x][y])
            # print(x,y)
            # sys.exit()

            for i in range(1, 10):
                if x - i < 0:
                    break
                elif state[x - i][y] == '.':
                    tempMove = list(move)
                    tempMove.append([x - i, y])

                    move_list.append(tempMove)
                else:
                    break

            # file=open("test.txt","w")
            # file.write(str(move_list))
            # file.close()
            # sys.exit()

            for i in range(1, 10):
                if x + i > 9:
                    break
                elif state[x + i][y] == '.':
                    tempMove = list(move)
                    tempMove.append([x + i, y])

                    move_list.append(tempMove)
                else:
                    break

            # file=open("test.txt","w")
            # file.write(str(move_list))
            # file.close()
            # sys.exit()

            for i in range(1, 10):
                if y - i < 0:
                    break
                elif state[x][y - i] == '.':
                    tempMove = list(move)
                    tempMove.append([x, y - i])

                    move_list.append(tempMove)
                else:
                    break

            # file=open("test.txt","w")
            # file.write(str(move_list))
            # file.close()
            # sys.exit()

            for i in range(1, 10):
                if y + i > 9:
                    break
                elif state[x][y + i] == '.':
                    tempMove = list(move)
                    tempMove.append([x, y + i])

                    move_list.append(tempMove)
                else:
                    break

            # file=open("test.txt","w")
            # file.write(str(move_list))
            # file.close()
            # sys.exit()

            for i in range(1, 10):
                if x - i < 0 or y - i < 0:
                    break
                elif state[x - i][y - i] == '.':
                    tempMove = list(move)
                    tempMove.append([x - i, y - i])

                    move_list.append(tempMove)
                else:
                    break

            # file=open("test.txt","w")
            # file.write(str(move_list))
            # file.close()
            # sys.exit()

            for i in range(1, 10):
                if x - i < 0 or y + i > 9:
                    break
                elif state[x - i][y + i] == '.':
                    tempMove = list(move)
                    tempMove.append([x - i, y + i])

                    move_list.append(tempMove)
                else:
                    break

            # file=open("test.txt","w")
            # file.write(str(move_list))
            # file.close()
            # sys.exit()

            for i in range(1, 10):
                if x + i > 9 or y - i < 0:
                    break
                elif state[x + i][y - i] == '.':
                    tempMove = list(move)
                    tempMove.append([x + i, y - i])

                    move_list.append(tempMove)
                else:
                    break

            # file=open("test.txt","w")
            # file.write(str(move_list))
            # file.close()
            # sys.exit()

            for i in range(1, 10):
                if x + i > 9 or y + i > 9:
                    break
                elif state[x + i][y + i] == '.':
                    tempMove = list(move)
                    tempMove.append([x + i, y + i])

                    move_list.append(tempMove)
                else:
                    break

            # file=open("test.txt","w")
            # file.write(str(move_list))
            # file.close()

        # file = open("test.txt", "w")
        # file.write(str(move_list))
        # file.close()

        for move in move_list:

            # change state
            new_board = self.board_copy(state)
            new_board[move[0][0]][move[0][1]] = '.'
            new_board[move[1][0]][move[1][1]] = self.str

            # generate arrow
            resultList.extend(self.shoot(move, new_board))

            # file=open("test.txt","w")
            # file.write(str(resultList))
            # file.close()
            # sys.exit()
        

        return resultList

    def shoot(self, move, state):
        '''
        queen shoot arrow
        +param-move: ex: queen move [0,0] -> [1,1] => move=[[0,0], [1,1]]
        +state: state after queen moved

        return:
        '''
        return_queue = deque()  # list of possible movement
        x = move[1][0]
        y = move[1][1]
        # self.board_print(state)
        # print("old position:", move[0][0], move[0][1])
        # print("new position", x, y)
        # sys.exit()

        for i in range(1, 10):
            if x - i < 0:
                break
            elif state[x - i][y] == '.':
                tempMove = list(move)
                tempMove.append([x-i, y])
                return_queue.append(tempMove)
                # pass
            else:
                break

        # file=open("test.txt","w")
        # file.write(str(return_queue))
        # file.close()
        # sys.exit()

        for i in range(1, 10):
            if x + i > 9:
                break
            elif state[x + i][y] == '.':
                tempMove = list(move)
                tempMove.append([x+i, y])
                return_queue.append(tempMove)
                # pass
            else:
                break

        # file=open("test.txt","w")
        # file.write(str(return_queue))
        # file.close()
        # sys.exit()

        for i in range(1, 10):
            if y - i < 0:
                break
            elif state[x][y-i] == '.':
                tempMove = list(move)
                tempMove.append([x, y-i])
                return_queue.append(tempMove)
                # pass
            else:
                break

        # file=open("test.txt","w")
        # file.write(str(return_queue))
        # file.close()
        # sys.exit()

        for i in range(1, 10):
            if y + i > 9:
                break
            elif state[x][y+i] == '.':
                tempMove = list(move)
                tempMove.append([x, y+i])
                return_queue.append(tempMove)
                # pass
            else:
                break

        # file=open("test.txt","w")
        # file.write(str(return_queue))
        # file.close()
        # sys.exit()

        for i in range(1, 10):
            if x - i < 0 or y - i < 0:
                break
            elif state[x - i][y - i] == '.':
                tempMove = list(move)
                tempMove.append([x - i, y - i])
                return_queue.append(tempMove)
                # pass
            else:
                break

        # file=open("test.txt","w")
        # file.write(str(return_queue))
        # file.close()
        # sys.exit()

        for i in range(1, 10):
            if x - i < 0 or y + i > 9:
                break
            elif state[x - i][y + i] == '.':
                tempMove = list(move)
                tempMove.append([x - i, y + i])
                return_queue.append(tempMove)
                # pass
            else:
                break

        # file=open("test.txt","w")
        # file.write(str(return_queue))
        # file.close()
        # sys.exit()

        for i in range(1, 10):
            if x + i > 9 or y - i < 0:
                break
            elif state[x + i][y - i] == '.':
                tempMove = list(move)
                tempMove.append([x + i, y - i])
                return_queue.append(tempMove)
                # pass
            else:
                break

        # file=open("test.txt","w")
        # file.write(str(return_queue))
        # file.close()
        # sys.exit()

        for i in range(1, 10):
            if x + i > 9 or y + i > 9:
                break
            elif state[x + i][y + i] == '.':
                tempMove = list(move)
                tempMove.append([x + i, y + i])
                return_queue.append(tempMove)
            else:
                break

        # file=open("test.txt","w")
        # file.write(str(return_queue))
        # file.close()
        # sys.exit()
        return return_queue

    def board_copy(self, board):
        new_board = [[]]*10
        for i in range(10):
            new_board[i] = [] + board[i]
        return new_board
